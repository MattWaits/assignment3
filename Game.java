/*
Names:   		Wingo, Tyler & Waits, Matt 
UT EIDs: 		tbw456 & mw26758
Hours Spent: 	15 hours
Section: 		16815
				EE422C-Assignment 2
Purpose:		
Logic Errors:	
SLOC:
*/

package assignment3;

//import java.io.InputStreamReader;
//import java.io.Reader;
import java.util.InputMismatchException;
import java.util.Scanner;
import javax.swing.JOptionPane;

//<<<<<<< HEAD:Game.java
/**
 * Contains the logic of the Mastermind game. 
 * 
 * @author Matt Waits, Tyler Wingo, Karen Gonzales
 * @version 2.0
 * @see Board Code Peg Color
 *
 */

//>>>>>>> 4961c7f1fdb56c6c89c42501c2e5ff47cc726cf1:src/assignment3/Game.java
public class Game
{
	/*change these lines to expand the game*/
	private final String VALID_COLORS = "BGOPRY";
	private final int NUM_COLORS = VALID_COLORS.length(); //Denotes number of different color pegs available	
	private final String VALID_COLORS_v2 = "Blue, Green, Orange, Purple, Red, or Yellow";
	private final int NUM_PEGS = 5;	//Denotes size of secret code & guesses
	private final int MAX_GUESSES = 15;
	
	
	/*attributes*/
	private Board theBoard;
	private int numGuesses=0;						//# of guesses made
	private boolean showSecret;						//if true, play in demo mode
	private Code secretCode;						//length NUM_PEGS; user must guess this value to win
	//private Code winningFeedbackCode;
	private StringBuilder tempFeedback;
	
	/**/
	
/*==============================================
 * 				CONSTRUCTORS
 * =============================================*/

	/**
	 * Creates a Game object which will run the Mastermind game. The parameter is used to 
	 * turn demo mode on/off. When in demo mode, the secret code the player is to guess 
	 * will be displayed.
	 * 
	 * @param demoMode	'True' if you want demoMode, 'false' is you do not.
	 */
	Game (boolean demoMode)
	{
		showSecret = demoMode;
		secretCode = new Code(VALID_COLORS, NUM_PEGS);
		theBoard = new Board(MAX_GUESSES, NUM_PEGS);
		tempFeedback = new StringBuilder();
		for (int q = 0; q<NUM_PEGS; q++)
		{
			tempFeedback.append('B');
		}
		//winningFeedbackCode = new Code(VALID_COLORS, tempFeedback.toString());
		System.out.println	("Welcome to Mastermind.\n\n"		+
							"-The computer will generate a secret code consisting of " + NUM_PEGS + " colored pegs.\n"	+
							"\t-The pegs MUST be one of " + NUM_COLORS + " colors: " + VALID_COLORS_v2 + "\n"				+
							"\t-Colors may be repeated in the secret code\n"	+
							"\t-You must guess the colored pegs and their order in the secret code using the capitalized first letter.\n"	+
							"-Feedback is displayed after each guess.\n"	+
							"\t-Each black feedback peg ('K') indicates a correct guess (color & location)\n"	+
							"\t-Each white feedback peg ('W') indicates a correct color but incorrect location\n"	+
							"\t-No feedback is given for fully incorrect pegs\n"	+
							"-Only the first letter of the color is used for input & output (e.g. 'B' for 'Blue')\n"	+
							"-You have " + MAX_GUESSES + " to figure out the secret code or you lose the game.\n"
							);
		return;
	}
	

/*==============================================
 * 				METHODS
 * =============================================*/
	/**
	 * In a nutshell, runs the game. Receives the input, logs the input, generates feedback for each guess, 
	 * logs that feedback, and updates the board graphic. Continues to run until code is guessed or player 
	 * runs out of turns. Calls other class methods to accomplish this. Contains exceptions which are 
	 * handled within.
	 * 
	 * @param readText	Scanner object used to read in player guesses.
	 */
	public void runGame(Scanner readText)
	{		
		while(this.numGuesses < MAX_GUESSES)
		{
			Code currentGuess = receiveGuess(readText);
			theBoard.logGuess(currentGuess);
			Code feedback = getFeedback(currentGuess);
			theBoard.logFeedback(feedback);
			
			if (currentGuess.compareTo(secretCode) == true)
			{
				//assert feedback.compareTo(winningFeedbackCode);								/*make sure that both methods of checking indicate a win*/
				if (showSecret == true)
				{
					System.out.println("\nSecret Code = " + secretCode.printTogetherCode());
				}
				System.out.println("Board layout:");
				theBoard.printBoard();
				System.out.println("Congratulations! You have won Mastermind.");
				break;																		/*exit while loop, no more guesses*/
			}
			
			this.numGuesses++;
		}
		if (this.numGuesses >= MAX_GUESSES)
		{
			if (showSecret == true)
			{
				System.out.println("\nSecret Code = " + secretCode.printTogetherCode());
			}
			System.out.println("Board layout:");
			theBoard.printBoard();
			System.out.println("\nGame over, loser.");
		}
		
		//readText.close();
		return;
	}
	/**
	 * Pulls in the player's guess, tests it for validity. Contains exceptions that are handled within.
	 * 
	 * @param codeScanner	Scanner object used to read player guess input.
	 * @return				Code object containing player's guess.
	 */
	private Code receiveGuess(Scanner codeScanner)
	{
		/*NEED TO ADD SAFEGUARDS & VALID INPUT CONFIRMATION*/
		
		if (showSecret == true)
		{
			System.out.println("\nSecret Code = " + secretCode.printTogetherCode());
		}
		System.out.println("Board layout:");
		theBoard.printBoard();
		
		boolean validInput = false;
		int checkPasses = 0;
		//String theGuess = " ";
		String theGuess = new String();
		while(!validInput)
		{
			checkPasses = 0;
			/*
			System.out.print("\nPlease enter guess of length " + NUM_PEGS + " containing only " + VALID_COLORS + ":");
			theGuess = codeScanner.nextLine(); //because by reading input in while loop threw 'theGuess' out of scope of return
			*/
			theGuess = (String) JOptionPane.showInputDialog(null, "Please enter guess of length " + NUM_PEGS + " containing only " + VALID_COLORS + ":");
				try
				{
					if(theGuess == null)
						throw new IllegalGuessException("Enter Code");
					for(int i = 0; i < theGuess.length(); i++)
					{
						for(int j = 0; j < NUM_COLORS; j++)
						{
							if(theGuess.charAt(i) == VALID_COLORS.charAt(j))
								checkPasses++;
						}	
					}

					
					if(checkPasses < NUM_PEGS)
						throw new InputMismatchException();					
					else if(checkPasses > NUM_PEGS)
						throw new ArithmeticException();
					else if(theGuess.length() != NUM_PEGS)
						throw new NumberFormatException();
						
					else
					{
						validInput = true;
						return new Code(VALID_COLORS, theGuess);
					}
				}
				catch(IllegalGuessException nl)
				{
					JOptionPane.showMessageDialog(null, "Invalid Input", "Error",JOptionPane.ERROR_MESSAGE);
				}
				catch(InputMismatchException em)
				{
					//System.out.println("\nInvalid Input: Guess contains invalid color.");
					JOptionPane.showMessageDialog(null, "Invalid Input", "Error",JOptionPane.ERROR_MESSAGE);
				}
				catch(ArithmeticException ae)
				{
					//System.out.println("\n!!LOGIC ERROR!! \nycheckPasses in ReceiveGuess method is > NUMBER_COLORS");
					JOptionPane.showMessageDialog(null, "Invalid Input", "Error",JOptionPane.ERROR_MESSAGE);
				}
				catch(NumberFormatException ne)
				{
					//System.out.println("\nInvalid Input: Guess should contain " + NUM_PEGS + " colors");
					JOptionPane.showMessageDialog(null, "Invalid Input", "Error",JOptionPane.ERROR_MESSAGE);
				}

		}
		return new Code(VALID_COLORS, theGuess);
	}
	/**
	 * Receives player's guess and generates feedback.
	 * 
	 * @param theGuess	Code object containing the player's guess.
	 * @return
	 */
	private Code getFeedback(Code theGuess)
	{
		Code secretCodeCopy = new Code(VALID_COLORS, secretCode.printTogetherCode());
		Code guessCopy = new Code(VALID_COLORS, theGuess.printTogetherCode());
		StringBuilder feedback = new StringBuilder();
		
		//Analyze for black pegs
		for (int i = 0; i<secretCodeCopy.getLength(); i++)
		{
				if (guessCopy.getPeg(i) == secretCodeCopy.getPeg(i))
				{
					guessCopy.setPeg(i, '-');
					secretCodeCopy.setPeg(i, '*');
					feedback.append('K');	//'K' for blacK peg
				}
		}
		
		//Analyze for white pegs
		for(int i = 0; i < secretCodeCopy.getLength(); i++)
		{
			for( int j = 0; j < secretCodeCopy.getLength(); j++)
			{
				if(guessCopy.getPeg(i) == secretCodeCopy.getPeg(j))
				{
					guessCopy.setPeg(i, '-');
					secretCodeCopy.setPeg(j, '*');
					feedback.append('W');	//'W' for White peg
				}
			}
		}
				
		return new Code("KW", feedback.toString());			/*note: game win is based on comparing guess to secretCode, and this is used as a backup verification of win*/
	}
	
	/**/
	
}
