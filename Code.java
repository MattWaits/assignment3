/*
Names:   		Wingo, Tyler & Waits, Matt 
UT EIDs: 		tbw456 & mw26758
Hours Spent: 	# hours
Section: 		16815
				EE422C-Assignment 2
Purpose:		
Logic Errors:	
SLOC:
*/

package assignment3;

import java.util.Random;

/**
 * Represents a sequence of pegs. The secret code the player is trying to find, the player's guess, as well as the
 * black and white peg feedback are all represented as Code objects. Each Code object has an array of Peg objects
 * and a length.
 * 
 * @author Tyler Wingo Matt Waits
 * @version 2.0
 * @see Peg
 */
public class Code
{
	private Peg[] pegArr;	//The pegs making up the code
	private int length;		//The number of pegs in the Code
	
	
	/*==============================================
	 * 				CONSTRUCTORS
	 * =============================================*/
	/**
	 * Creates a Code object of Pegs with randomized color.
	 * 
	 * @param colorIDs		String that lists valid colors 
	 * @param codeLength	Number of pegs in the code.
	 */
	Code(String colorIDs, int codeLength)												/*generate random code*/
	{
		length = codeLength;
		Random randGenerator = new Random();
		pegArr = new Peg[codeLength];
		for (int i = 0; i<codeLength; i++)
		{
			/*create Peg with random color index ranging from 0 to [# of colors-1]*/
			pegArr[i] = new Peg( colorIDs, randGenerator.nextInt(colorIDs.length()) );		
		}
	}
	
	/**
	 * Creates a Code object of Pegs of specific color. Generally used to convert a String to a Code object.
	 * (i.e. Code("BRYOGP", "BBBB") creates a Code object with four Blue pegs)
	 * 
	 * @param colorIDs		String that lists valid colors.
	 * @param codeContents	String where each char is a color of peg in code to be generated. Assumes codeContents consists of valid colors
	 */
	Code(String colorIDs, String codeContents)											/*convert given String to Code*/
	{																					/*assumes codeContents only consists of valid color codes*/
		length = codeContents.length();
		/*assert that codeContents is valid*/
//		boolean validColor;
//		for(int j = 0; j<length; j++)													/*check each letter in codeContents*/
//		{
//			validColor = false;
//			for (int k = 0; k<colorIDs.length(); k++)
//			{
//				if (codeContents.charAt(j) == colorIDs.charAt(k))
//				{
//					validColor = true;													/*letter is only valid if it at least equals one of the valid color codes*/
//				}
//			}
//			assert validColor == true;
//		}
		/**/
		
		pegArr = new Peg[codeContents.length()];										/*create array of pegs of the specified length*/
		for (int i = 0; i<codeContents.length(); i++)									/*assign color to each peg*/
		{
			pegArr[i] = new Peg(codeContents.charAt(i));
		}
	}
	/**/

	
	/*==============================================
	 * 				METHODS
	 * =============================================*/
	/**
	 * Returns a StringBuilder representation of the Code object with spaces between each char/peg/color.
	 * 
	 * @return	StringBuilder representation of the Code object with spaces between each char/peg/color.
	 */
	public String printSpacedCode()
	{
		StringBuilder returnCode = new StringBuilder();
		for(int i=0; i<length; i++)
		{
			returnCode.append( this.pegArr[i].getColor() );							/*iterate through code, printing each char color indicator (e.g. 'Y' for "Yellow")*/	
			returnCode.append(" ");
		}
		return returnCode.toString();
	}
	/**
	 * Returns a StringBuilder representation of the Code object with no spaces between each char/peg/color.
	 * 
	 * @return	StringBuilder representation of the Code object with no spaces between each char/peg/color.
	 */
	public String printTogetherCode()
	{
		StringBuilder returnCode = new StringBuilder();
		for(int i=0; i<length; i++)
		{
			returnCode.append( this.pegArr[i].getColor() );							/*iterate through code, printing each char color indicator (e.g. 'Y' for "Yellow")*/	
		}
		return returnCode.toString();
	}
	/**
	 * Used to retrieve color information on an individual peg within the Code object.
	 * 
	 * @param index	Index of peg you wish to utilize [0 - sizeOfCode-1].
	 * @return		char Color of peg at index specified in parameter.
	 */
	public char getPeg(int index)
	{
		//assert index<this.length;				//make sure valid index in code
		return this.pegArr[index].getColor();
	}
	/**
	 * Used to set/change color of an individual peg within the Code object.
	 * 
	 * @param index	Index of peg you wish to set/change color of [0 - sizeOfCode-1].
	 * @param col	Color you wish to change peg to. Assumes 'col' represents a valid color.
	 */
	public void setPeg(int index, char col)
	{
		this.pegArr[index].setColor(col);
		return;
	}
	/**
	 * Gives the number of pegs found in the Code object.
	 * 
	 * @return	Number of pegs in this Code object.
	 */
	public int getLength()
	{
		return length;
	}
	/**
	 * Compares the color of Peg objects in this Code with that of the one in the input parameter sequentially.
	 * Returns 'true' if the codes are the same, 'false' if they are not.
	 * 
	 * @param otherCode	The Code object to be compared with this one.
	 * @return			'True' if the codes are the same, 'false' if they are not.
	 */
	public boolean compareTo(Code otherCode)
	{
		boolean compareRes = true;
		//assert otherCode.getLength() == this.length;
		for(int i = 0; i<this.length; i++)
		{
			if(otherCode.pegArr[i].getColor() !=this.pegArr[i].getColor())
			{
				compareRes = false;
			}
		}
		return compareRes;
	}

	
}

