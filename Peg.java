/*
Names:   		Wingo, Tyler & Waits, Matt
UT EIDs: 		tbw456 & mw26758
Hours Spent: 	# hours
Section: 		16815
				EE422C-Assignment 2
Purpose:		
Logic Errors:	
SLOC:
*/

package assignment3;
//Phillip

/**
 * Represents a "peg" in the Mastermind game. A peg has a Color object which defines the color of the peg.
 * 
 * @author 	Tyler Wingo, Matt Waits
 * @version	2.0
 *
 */
public class Peg
{
	
	private Color color;
	
	
	/*==============================================
	 * 				CONSTRUCTORS
	 * =============================================*/
	/**
	 * Creates a Peg by taking a string of letters representing all valid colors and an number representing the index
	 * of the string pertaining to the color you wish the peg to be.
	 * 
	 * @param colorIDs all currently valid colors, all caps
	 * @param colorSelect Index of specific color in colorIDs
	 */
	Peg(String colorIDs, int colorSelect)		//receives string of all color IDs & index of this color
	{
		color = new Color(colorIDs.charAt(colorSelect));
		//color = colorIDs.charAt(colorSelect);	//e.g. input ["BGOPRY", 3] would set color to 'P'
	}
	
	/**
	 * Creates a Peg of color specified by input parameter.
	 * 
	 * @param col Color of peg being created (first letter, capitalized).
	 */
	Peg(char col)
	{
		color = new Color(col);
	}
	
	/**
	 * Creates a Peg of color specified by input parameter.
	 * 
	 * @param c Color object used to define color of the peg.
	 */
	Peg(Color c)
	{
		color = new Color(c.getColor());
	}
	
	
/*==============================================
 * 				METHODS
 * =============================================*/
	
	/**
	 * Retrieves the color of the peg.
	 * 
	 * @return char Color of peg.
	 */
	public char getColor()
	{
		return color.getColor();
	}
	
	
	/**
	 * Sets the color of the peg to that of input parameter.
	 * 
	 * @param col Color being set.
	 */
	public void setColor(char col)
	{
		color.setColor(col);
		return;
	}
	
}
