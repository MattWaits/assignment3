/*
Names:   		Wingo, Tyler & Waits, Matt 
UT EIDs: 		tbw456 & mw26758
Hours Spent: 	# hours
Section: 		16815
				EE422C-Assignment 2
Purpose:		
Logic Errors:	
SLOC:
*/

package assignment3;


/**
 * Conceptually and graphically represents the Matermind game board. Contains the history of each of the player's guesses
 * as well as the feedback received for each.
 * 
 * @author	Matt Waits Tyler Wingo Karen Gonzales
 * @version	2.0
 * @see 	Code
 */
public class Board
{
	private Code[] guessRecord;									/*size is allocated in constructor*/
	private Code[] feedbackRecord;
	
	int codeLength, guessIndex, feedbackIndex, numberRows;
	
	/*==============================================
	 * 				CONSTRUCTORS
	 * =============================================*/
	/**
	 * Creates a board object where codes will consist of 'sizeRow' pegs and you will be given 'numRows' guesses
	 * 
	 * @param numRows	The limit number of guesses you will have to figure out the secret code.
	 * @param sizeRow	The number of pegs contained in each secret code, guess, and feedback
	 */
	Board(int numRows, int sizeRow)
	{
		guessRecord = new Code[numRows];				/*create array so each element can represent code for one row*/
		feedbackRecord = new Code[numRows];
		codeLength = sizeRow;
		numberRows = numRows;
		guessIndex = 0;									/*keeps track of location in record*/
		feedbackIndex = 0;
	}
	
	
	/*==============================================
	 * 				METHODS
	 * =============================================*/
	/**
	 * Used to save the player's guess into the history.
	 * 
	 * @param currentGuess	The player's guess to be logged.
	 */
	public void logGuess(Code currentGuess)
	{
		guessRecord[guessIndex] = currentGuess;
		guessIndex = guessIndex + 1;
		return;
	}
	
	/**
	 * Used to save the player's feedback into the history.
	 * 
	 * @param currentFeedback	The player's feedback to be logged.
	 */
	public void logFeedback(Code currentFeedback)
	{
		feedbackRecord[feedbackIndex] = currentFeedback;
		feedbackIndex = feedbackIndex + 1;
		return;
	}
	
	/**
	 * Prints an ASCII graphical representation of the gameboard. At first the board will be comprised of rows of empty slots,
	 * but as the player makes guesses the board will be filled with the guess history and the feedback for each guess.
	 */
	public void printBoard()							/*print board from newest to oldest rows (top to bottom)*/
	{
		//assert guessIndex == feedbackIndex;				/*same number of guesses should have been logged as feedbacks*/
		int i = numberRows;
		while (i>guessIndex)
		{
			//assert (guessRecord[i] == null && feedbackRecord[i] == null);
			System.out.print("| ");
			for (int zz=0; zz<codeLength; zz++)
			{
				System.out.print("_ ");
			}
			System.out.print("| ");
			for (int zz=0; zz<codeLength; zz++)
			{
				System.out.print("_ ");
			}
			System.out.println("|");
			i--;
		}
		while(i>0)
		{
			System.out.print("| " + guessRecord[i-1].printSpacedCode() + "| " + feedbackRecord[i-1].printSpacedCode());
			for (int j=feedbackRecord[i-1].getLength(); j<codeLength; j++)
			{
				System.out.print("_ ");
			}
			System.out.println("|");
			i--;
		}
		return;
	}
}
