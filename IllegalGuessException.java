package assignment3;

import javax.swing.JOptionPane;

/**
 * Signifies that a player's guess does not conform with expected values.
 * (i.e. contains invalid colors, is of improper length)
 * 
 * @author Karen Gonzales
 *
 */
public class IllegalGuessException extends RuntimeException
{
	public IllegalGuessException(String message)
	{
		super(message);
	}
}
