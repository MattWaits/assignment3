/*
Names:   		Wingo, Tyler & Waits, Matt 
UT EIDs: 		tbw456 & mw26758
Hours Spent: 	# hours
Section: 		16815
				EE422C-Assignment 2
Purpose:		
Logic Errors:	
SLOC:
*/

package assignment3;

import java.util.InputMismatchException;
import java.util.Scanner;

import javax.swing.JOptionPane;

public class A2Driver
{
	public static void main(String args[])
	{
		if (args.length != 1 || (!args[0].equals("false") && (!args[0].equals("true"))) )
		{
			System.err.println("Error: Incorrect command line arguments.\nFix with \"true\" or \"false\" to determine demoMode\nExiting program.");
			System.exit(-1);
		}
		
		boolean quitter = false;
		boolean demoMode = false;
		Scanner scanMan = new Scanner(System.in);
		String playWithMe = "";
				
		while(!quitter)
		{

			//JOptionPane.showMessageDialog(null, "test");
			System.out.println("\nWould you like to play a game?");
			
				try
				{
					playWithMe = scanMan.nextLine();
					if(validYNInput(playWithMe))
					{
						if(playWithMe.charAt(0) == 'n' || playWithMe.charAt(0) == 'N')
							quitter = true;
						else
						{
							
							if (args[0].equals("true"))
							{
								demoMode = true;
							}
							else if (args[0].equals("false"))
							{
								demoMode = false;
							}
							Game theGame = new Game(demoMode);
							theGame.runGame(scanMan);
						}
					}
					else
						throw new InputMismatchException();
				}
				catch(InputMismatchException ime)
				{
					System.out.println("\nInvalid Input! Enter Y or N");
				}
				
		}
		
		System.out.println("\nFINE! LEAVE THEN!! XP");
		scanMan.close();		
	}
	
	public static boolean validYNInput(String s)
	{
		if(	   s.charAt(0) == 'y'
			|| s.charAt(0) == 'Y'
			|| s.charAt(0) == 'n'
			|| s.charAt(0) == 'N')
			return true;
		else
			return false;
	}
}
