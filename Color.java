package assignment3;


/**
 * Contains a char the represents a specific color. Used to define the color of a Peg object.
 * @see Peg
 * 
 * @author 	Matt Waits
 * @version	2.0
 *
 */
public class Color {
	
	private char color;
	
/*==============================================
 * 				CONSTRUCTORS
 * =============================================*/
	/**
	 * Creates a Color object with color value 'c'
	 * 
	 * @param c Defines the color being represented by the first letter in its name (capitalized)
	 */
	Color(char c)
	{
		color = c;
	}

	
/*==============================================
 * 				METHODS
 * =============================================*/
	/**
	 * Returns the color value of the object. (i.e. 'R' for Red, 'B' for Blue,...)
	 * 
	 * @return First letter of name of color, capitalized
	 */
	public char getColor()
	{
		return color;
	}
	/**
	 * Used to change the color of the object. 
	 * 
	 * @param c	The color this object is to represent.
	 */
	public void setColor(char c)
	{
		color = c;
	}
}
